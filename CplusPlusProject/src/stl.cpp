/*
 * stl.cpp
 *
 *  Created on: 2 avr. 2021
 *      Author: STuser
 */

#include <iostream>
#include <vector>
#include <array>
#include <list>
#include <queue>
#include <forward_list>
#include <map>
#include <algorithm>
#include <memory>
#include "stl.hpp"

using namespace std;

bool assezLong(string s){return s.size()>=7;}
void collections()
{
	cout<<"les collections :"<<endl;
	vector<string> titres = {"bobo","toto","tata"};
	titres.push_back("koko");
	titres.insert(titres.begin(), "Best of Hom�re");
	//titres[0] = "L'" + titres[0];
	cout<<titres.size()<<" titres"<<" "<<endl;
	/******************************************/

	for (unsigned i = 0; i<titres.size(); i++)
		cout<<titres[i];

	/******************************************/
	for (auto it=titres.begin(); it!=titres.end();it++)
		cout<<*it<<endl;

	/******************************************/
	for (const string& s : titres )
		cout<<s<<" ";

	/******************************************/

	vector<string> titresPairs;
	for (auto it=titres.begin(); it<titres.end(); it+=2)
		titresPairs.push_back(*it);

	array<string,3> titresCourts {"Jo", "Frame", "Si"};

	list<string> auteurs {"Homere","Proust", "Zola"};

	deque<long> retours {2356,458,25698};

	forward_list<string> autresAuteurs {"gogo", "bobo"};

	vector<string> titresLongs(10);
	//copy_if(titres.begin(), titres.end(), titresLongs.begin(), assezLong);
	//lambda
	copy_if(titres.begin(), titres.end(), titresLongs.begin(),
			[](string s){return s.size()>=8; } );
	cout<<endl<<"Titres longs : "<<endl;
	for(const string& s : titresLongs)
		cout<<s<<" ";

	bool unY = any_of(titres.begin(),titres.end(),
			[](string s){return s.find('y',0)!=string::npos;});
	cout<<endl<<"un ydnas l'un des titres? " <<unY<<endl;

	//est-ce que tous les titres font moins de 8 caract�res
	bool mystere = all_of(titres.begin(),titres.end(),
			[](string s){return s.size() < 8; } );
	cout<<endl<<"un mystere ? " <<mystere<<endl;
}


void ensembles()
{
	map<unsigned,unsigned>nouveautesParAnnee {{2015,23},{2019,11},{2020,32}};
	if (nouveautesParAnnee.count(2019))
		cout<<"2019 : "<<nouveautesParAnnee[2019]<<" nouveaut�s" <<endl;
	map<unsigned, array<unsigned,12>> nouveautzParAnneeEtMois;
	nouveautzParAnneeEtMois[2020][3] = 0;
}

void pointeurIntelligent()
{
	cout<<"Smart pointers : "<<endl;
	string* s1 = new string("Antenne de borderoufe");
	cout<<*s1<<endl;
	delete s1;

	unique_ptr<string> s2(new string("Bibliotheque centrale"));
	cout<<*s2<<endl;

}


