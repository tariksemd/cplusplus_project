/*
 * fonction.cpp
 *
 *  Created on: 30 mars 2021
 *      Author: STuser
 */


#include "fonction.hpp"
#include <iostream>
#include <functional>
using namespace std;



void montreOuvrage(const string titre, const string auteur, const unsigned pages) //la fonction n'a pas le dt de modifier les argus
{
	cout<<"**"<<titre<<"** de "<<auteur<<" ("<<pages<<" )"<<endl;
}
void parametres(void)
{
	cout<<"bonjour Tarik ����"<<endl;
}
void montreInfoBibli(int nbInfo, ...)
{

}

void variableFoknction()
{
	//function<void(string,string,unsigned)> f1 = montreOuvrage; //utilisation si le nom de la fonction est tres long
	auto f1 = montreOuvrage;
	f1("le manuel de l'electronique", "ulgo", 23);

	function<void()> f2 = bind(montreOuvrage,"le manuel de l'electronique de puissance", "toto", 100);
	f2();

	function<void(unsigned)> f3 = bind(montreOuvrage,"le manuel de l'electronique de puissance de Tarik", "fofo",
			placeholders::_1);
	f3(456);

	function<void(string)> f4 = bind(montreOuvrage, placeholders::_1, "Cris", 25);
	f4("Asterix et Cleopatre");

	function<void(unsigned,string)> f5 = bind(montreOuvrage, placeholders::_2, "Xavier", placeholders::_1);
	f5(42, "La Zizanie");

}
