/*
 * stl.hpp
 *
 *  Created on: 2 avr. 2021
 *      Author: STuser
 */

#ifndef STL_HPP_
#define STL_HPP_

void collections();
void ensembles();
void pointeurIntelligent();
#endif /* STL_HPP_ */
