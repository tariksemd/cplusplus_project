/*
 * operateurs.cpp
 *
 *  Created on: 31 mars 2021
 *      Author: STuser
 */


#include <iostream>
#include "operateurs.hpp"
using namespace std;



Rayon::Rayon() : Rayon("",0)
{

}
Rayon::Rayon(string name, unsigned index) : nom(name), ouvrage(index)
{

}
void Rayon::afficher() const
{
	cout<<"le nom  est :"<<nom<<" l'ouvrage est :"<<ouvrage<<endl;
}
Rayon Rayon::operator+(const Rayon& src) const
{
	Rayon res;
	res.nom = this->nom +"  "+ src.nom;
	res.ouvrage = this->ouvrage + src.ouvrage;

	return res;

}
bool Rayon::operator!=(const Rayon& src) const
{

	if ((src.nom == this->nom) &&(src.ouvrage == this->ouvrage) )
	{
		return false;
	}
	else
	{
		return true;
	}


}
bool Rayon::operator==(const Rayon& src) const
{

	return !(*this!=src);

}
void Rayon::operator()() const
{

	this->afficher(); // ATTENTION! etre sur que afficher ne modifie pas le code !
						// ajouter const a la fonction afficher

}
void visibliteEtOperateur()
{
	cout<<"Visbilité et operateurs"<<endl;
	Rayon r1("Electronique",12);
	Rayon r2("Philosophi",123);
	//r1.nom = "Histoire";
	//r1.ouvrage = 4589;

	//cout<<r1.nom<<endl;
	r1.afficher();
	r2.afficher();
	r1.ouvrage += 100;
	r1.afficher();
	Rayon r3;
	r3= r1+r2;
	r3.afficher();
	cout<<(r3!=r2)<<endl;
	cout<<(r3==r2)<<endl;
	r3();
}
