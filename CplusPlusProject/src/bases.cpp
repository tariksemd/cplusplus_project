/*
 * bases.cpp
 *
 *  Created on: 30 mars 2021
 *      Author: STuser
 */

#include <iostream>
#include <array>
using namespace std;

//commit 1

//commit 2

//commit 2
int Affichage_des_variables(void)
{

	//
	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!
	int ouvrage = 254'000;
	long long entrees = 342'023;
	long data = 1523;
	char antennes = 8;
	int cds = 82300;
	short rayons = 460;
	float jauge = 2.6;
	double capteur = 2.8f;
	cout <<"l ouvrage qui egale "<< ouvrage<<" est sa taille int:"<<sizeof(ouvrage)<<endl
			<<"l entrees qui egale "<< entrees<<" est sa taille long long:"<<sizeof(entrees)<<endl
			<<"l entrees qui data "<< data<<" est sa taille long:"<<sizeof(data)<<endl
			<<"l antennes qui egale "<< antennes<<" est sa taille char:"<<sizeof(antennes)<<endl
			<<"l cds qui egale "<< cds<<" est sa taille int:"<<sizeof(cds)<<endl
			<<"l rayons qui egale "<< rayons<<" est sa taille short:"<<sizeof(rayons)
			<<"l jauge qui egale "<< jauge<<" est sa taille float:"<<sizeof(jauge)<<endl
			<<"l capteur qui egale "<< capteur<<" est sa taille double:"<<sizeof(capteur)
			<< "Hello imane C'est Tarik"
			<<endl;
	int16_t i16_max_auteurs = INT16_MAX;
	int16_t i16_min_auteurs = INT16_MIN;
	cout<<"la valeur max est min de auteurs sont "<<i16_max_auteurs<<" et "<<i16_min_auteurs<<endl;

	cout<<"/****************************************************/"<<endl;

	int64_t i64_max_auteurs = INT64_MAX;
	int64_t i64_min_auteurs = INT64_MIN;
	cout<<"la valeur max et min de auteurs sont "<<i64_max_auteurs<<" et "<<i64_min_auteurs<<endl;

	cout<<"/****************************************************/"<<endl;
	bool present = true;
	string nom = "Tarik";
	/* une optimisation de calcule avec le &&(op) et le &*/
	bool assez_de_rayon = (rayons>100) & (rayons<500);
	bool assez_de_rayon1 = (rayons>100) && (rayons<500); //ici si la premiere est faux donc le resu est faux
	int size = 14;
	float tab[size];
	tab[13] = 1.2f;

	for (auto j : tab)
		cout<<j<<" "<<endl;

	std::array<int,5> tabdelivre[] = {2300,1432,3698,1222,158};
	int plus_petit_rayon, plus_grand_rayon =0;
	for (int i=0;i<5;i++)
	{
		//this comment is added to learn the merge and rebase aspect
		if(tabdelivre[i]>tabdelivre[plus_grand_rayon])
		{
			plus_grand_rayon = i;
		}
		if(tabdelivre[i]<tabdelivre[plus_grand_rayon])
		{
			plus_petit_rayon = i;
		}

	}
	cout<<"Rayon avec le moins de livre "<<plus_petit_rayon+1<<endl;
	cout<<"Rayon avec le plus de livre "<<plus_grand_rayon+1<<endl;
	return 0;
}
