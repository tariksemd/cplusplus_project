/*
 * operateurs.hpp
 *
 *  Created on: 31 mars 2021
 *      Author: STuser
 */

#ifndef OPERATEURS_HPP_
#define OPERATEURS_HPP_

#include <iostream>
using namespace std;
void visibliteEtOperateur();

class Rayon {

	string nom;
	unsigned ouvrage;
public:
	Rayon();
	Rayon(string, unsigned);
	void afficher() const;
	Rayon operator+(const Rayon&) const;
	bool operator!=(const Rayon&) const;
	bool operator==(const Rayon& src) const;
	void operator()() const;

	friend void visibliteEtOperateur();

};
#endif /* OPERATEURS_HPP_ */
