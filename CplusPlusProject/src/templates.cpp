/*
 * templates.cpp
 *
 *  Created on: 1 avr. 2021
 *      Author: STuser
 */

#include <iostream>
#include <vector>
#include "templates.hpp"
using namespace std;

template<typename T> const T PI = T(3.14156895426);
template<typename T> void afficher(T x, T y){cout<<x<<" et "<<y<<endl;}
template<typename T> T getX() {cout<<"x ? "; T x; cin>>x; return x;}
template<typename T> T somme(T x, T y, T z){ return x+y+z;}
template<class T1, class T2> decltype(auto) multi(T1 a, T2 b){ return a*b;}


template<typename T> struct Conteneur {
	T v;
	Conteneur(){}
	Conteneur(T arg): v(arg) {}
	//bool egalApproximatif(int n) {return (n>v-1) && (n<v+1);}
	template<class T1> bool egalApproximatif(T1 n) {return (n>v-1) && (n<v+1);}
};

template<> struct Conteneur<string> { // une spécilaisaiton de la class conteneur
	string v;
	Conteneur(): v("inconnue"){}
	Conteneur(string arg): v(arg) {}
	//bool egalApproximatif(int n) {return (n>v-1) && (n<v+1);}
	//template<class T1> bool egalApproximatif(T1 n) {return (n>v-1) && (n<v+1);}
};
// spécialisation partielle :
/*
template<typename T1, typename T2> struct S {....
template<typename T1> struct S<T1,srting> {....
template<typename T2> struct S<int,T2> {....
template<> struct S<int,double> {....
*/
namespace formation_ts {
template<int V> unsigned mulitplieur(unsigned x) {return x*V;}
}
void templates()
{
	cout<<"Templates :"<<endl;
	vector<int> v1;
	float pif = PI<float>;
	cout<<"float "<<pif<<endl;
	cout<<"double "<<PI<double><<endl;
	cout<<"unsigned "<<PI<unsigned><<endl;
	afficher(3,56);
	afficher("Semrade","Tarik");
	afficher<double>(6,8.3);
	//double res =  getX<double>();
	//cout<<getX<double>()<<endl;
	cout<<somme<string>("a","b","c")<<" easy as "<<somme<unsigned>(1,2,3)<<endl;
	cout<<multi(9,45)<<" "<<multi(9.2,45)<<"  "<<multi(45,9.2)<<endl;

	Conteneur<float> c1;
	c1.v = 2.3f;

	Conteneur<float> c2(3.5f);
	cout<<"Conteneurs :"<<c1.v<<" "<<c2.v<<" "<< c2.egalApproximatif(4.5)<<endl;

	Conteneur<string> c3;
	cout<<"Conteneurs :"<<c3.v<<endl;

	cout<<"la multiplication de 3 *4 égale :"<<formation_ts::mulitplieur<4>(5)<<endl;
}


