/*
 * pointeurs.cpp
 *
 *  Created on: 30 mars 2021
 *      Author: STuser
 */
#include <iostream>
using namespace std;

#include "pointeurs.hpp"

void pointeurAndTables()
{
	cout<<"Pointeurs"<<endl;
	unsigned a = 27;
	unsigned *ptr_a = &a;

	cout<<a<<", "<<ptr_a<<endl;

	unsigned int **adr = &ptr_a;
	cout<<"@ pa dans adr"<<", "<<adr<<endl;

	float tf[5] ={0, 2, 4, 6, 8};
	float* pf = nullptr;
	if (pf!=nullptr)
		cout<<"Valeur derri�re pf : "<<*pf<<endl;

	//pf=tf;
	if (pf!=nullptr)
		cout<<"Valeur derri�re pf : "<<*pf<<endl;
	float sum = 0;
	for(pf=tf; pf<tf+5 ;pf++)
	{
		sum = sum + *pf;
	}
	cout<<"Toto  "<<sum<<endl;
}
unsigned minAnee(unsigned *annees, unsigned taille)
{
	unsigned min = 2100;
	for(unsigned i =0; i<taille; i++)
	{
		if(annees[i]<min)
		{
			min = annees[i];
		}
	}
	return min;
}
void tableuxEtFonctions()
{
	cout<<"Tableaux et fonctions"<<endl;
	unsigned antennes[] {1967, 1966, 1982, 2002, 1987, 2004};
	cout<<" Ouveture : "<< minAnee(antennes,6)<<endl;

}

void affichebleuRays(unsigned& bluerays)
{
	bluerays = 10 * unsigned(bluerays/10);
	cout<<"** Bleuerays : "<<bluerays<<endl;
}
string metterEnForme(string& s)
{
	string res = " _ "+s+" _ ";
	return res;
}
string metterEnForme(string&& s)
{
	s = " _ "+s+" _ ";
	return s;
}
void reference()
{
	unsigned bluerays = 3245;
	unsigned& br2 = bluerays;		// c est un alias
	bluerays+=481;
	cout<<"Bleurays :"<<br2<<endl;
	affichebleuRays(bluerays);
	cout<<"Bleurays :"<<br2<<endl;
	string titre = "C++ avanc� (2eme edition)";
	string misEnForme = metterEnForme(titre);
	cout<<titre<<" => "<<misEnForme<<endl;
	cout<<metterEnForme("la programation est facile")<<endl;
}
