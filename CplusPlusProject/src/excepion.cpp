/*
 * excepion.cpp
 *
 *  Created on: 1 avr. 2021
 *      Author: STuser
 */
#include <iostream>

using namespace std;

unsigned getBiblitothecaires(int visiteurs) noexcept
{
	return (1 + visiteurs / 25);
}

void exceptions() noexcept  // les exceptions sont ratrapés
{
	cout<< "Exceptions : "<<endl;
	int visiteurs;
	cout<<"Visiteurs ?";
	cin>>visiteurs;
	unsigned tables = 14;
	try
	{
		if(visiteurs == 0)
			throw 0; // les exception sont lancees par valeur et attrapees par reference

		if(visiteurs < 0)
			throw out_of_range("Trop petit");

		unsigned tablesParVisiteur = tables / visiteurs;
		cout<<"Tables par visiteur :"<<tablesParVisiteur<<endl;
		cout<<"Bibliothécaire nécessaires : "<<getBiblitothecaires(visiteurs)<<endl;

	}
	catch (int codeErreur)
	{
		cout<<"Code d'erreur"<<codeErreur<<endl;
	}
	catch (const out_of_range& e)
	{
		cout<<"Exception - hors limites- "<<e.what()<<endl;
	}
	catch (...)
	{
		cout<<"Division par zero n'est pas autorisée"<<endl;
	}
	if(noexcept(getBiblitothecaires(visiteurs)))
	{
		cout<<"Biblitothecaires :"<<getBiblitothecaires(visiteurs)<<endl;
	}


}
