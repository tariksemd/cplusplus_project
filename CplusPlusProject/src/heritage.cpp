/*
 * heritage.cpp
 *
 *  Created on: 1 avr. 2021
 *      Author: STuser
 *
 */
//this comment is added to learn the merge and rebase aspect
//this comment is added to learn the merge and rebase aspect
//this comment is added to learn the merge and rebase aspect
//this comment is added to learn the merge and rebase aspect

#include <iostream>
using namespace std;

class Affichable {
public:
	virtual void afficher() = 0;
};
/**
 *
 *
 * **/
class Vehicule : public Affichable /* pas h�ritable utiliser => final */
{
public:
	/**
	 *
	 * C++11 forcer le constructeur par  defaut.
	 * et ne pas faire de constructeur par recopie.
	 */
	Vehicule() = default;
	Vehicule(const Vehicule&) = delete;

	Vehicule(string n): nom(n) {}
	virtual void afficher() { cout<< nom << " ";}
	void afficherJoli() { cout<<"##";afficher();cout<<"##";}
protected:
	void videNom() {nom = "";}
	string nom;
};
/**
 *
 *
 * **/
class Camionnette : virtual public Vehicule {
public:
	unsigned livres;
	Camionnette(string n, unsigned l): Vehicule(n), livres(l){}
	//le mot override permet de bien verifier que la fonction est redefinie
	/*
	 * Pour empecher la virtualisation utiliser finale
	 */
	void afficher() /*final*/ override {Vehicule::afficher(); cout<<" ("<< livres<< ")";}
	using Vehicule::videNom;//fend public puisqu'on est dans une zone public.
};

/**
 *
 *
 * **/
class Bus : virtual public Vehicule{
public:
	unsigned accueil;
	Bus(string n, unsigned a) : Vehicule(n),accueil(a) { }
	void afficher() {Vehicule::afficher();cout<<"(max : "<<accueil<<" ) ";}
};
/**
 *
 *
 * **/
class Bibliobus : public Camionnette, public Bus{
public:
	bool actif;
	Bibliobus(string n, unsigned l, unsigned acc, bool act) : Vehicule(n) , Camionnette(n,l), Bus(n, acc), actif(act) {}
#if 0
		Camionnette(n,l); // ATTENTION: appel tardif
		Bus(acc);
		actif(act);
#endif
	void afficher()
	{
		Camionnette::afficher();
		Bus::afficher();

		if(actif)
			cout<<" * actif * ";
		else
			cout<<" * inactif * ";
	}
//this comment is added to learn the merge and rebase aspect
};
/**
 *
 *
 * **/
void heritageSimple()
{
	cout<< " Heritage : "<<endl;
	Vehicule v1("Peugeot 404");
	v1.afficher();
	v1.afficherJoli();
	cout<<endl;
	Camionnette Man("Man", 1000);
	Man.afficher();
	Man.afficherJoli();
	cout<<endl;
	Man.videNom();
	Man.afficherJoli();
	Bibliobus b("CAT", 1000, 10, false);
	b.afficher();
	b.Camionnette::afficherJoli();
	// 28, 36, 36, 48
    cout << sizeof(Vehicule) << ", " << sizeof(Camionnette) << ", " <<
        sizeof(Bus) << ", " << sizeof(Bibliobus) << endl;
}
/***
//           Vehicule   Vehicule                 Vehicule
//                 \      /                        /  \
//         Camionnette  Bus       =>     Camionnette  Bus
//                  \   /                         \   /
//                 Bibliobus                    Bibliobus
*/
