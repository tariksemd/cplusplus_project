//============================================================================
// Name        : CplusPlusProject.cpp
// Author      : Tarik
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "bases.hpp"
#include "fonction.hpp"
#include "pointeurs.hpp"
#include "objects.hpp"
#include "operateurs.hpp"
#include "heritage.hpp"
#include "exception.hpp"
#include "templates.hpp"
#include "stl.hpp"
using namespace std;


int main ()
{
	//Affichage_des_variables();
	//parametres();
	//montreOuvrage("le bon coulmage", "zozo", 23);
	//montreOuvrage("le bon coulmage", "zozo", 23);
	//montreOuvrage("Tarik");
	//variableFoknction();
	//pointeurAndTables();
	//tableuxEtFonctions();
	//reference();
	//basesDeLobject();
	//objectsComplets();
	//formeCanonique();
	//visibliteEtOperateur();
	//heritageSimple();
	//exceptions();
	//templates();
	//collections();
	//ensembles();
	pointeurIntelligent();
}
