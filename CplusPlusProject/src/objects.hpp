/*
 * objects.hpp
 *
 *  Created on: 31 mars 2021
 *      Author: STuser
 */

#ifndef OBJECTS_HPP_
#define OBJECTS_HPP_

using namespace std;
void basesDeLobject(void);
void objectsComplets();
void formeCanonique();

struct Employe {
	string nom;
	int id;

	Employe();
	Employe(string);
	Employe(string, int);
	void afficher();
	~Employe();
};

struct Equipe {
	Employe* employes = nullptr;
	unsigned taille = 0;

	Equipe();
	Equipe(unsigned);
	Equipe(const Equipe&);
	Equipe(Equipe&&);
	Equipe& operator = (const Equipe&);
	~Equipe();
	void afficher();
};

#endif /* OBJECTS_HPP_ */
