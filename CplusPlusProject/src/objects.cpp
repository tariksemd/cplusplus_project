/*
 * objects.cpp
 *
 *  Created on: 31 mars 2021
 *      Author: STuser
 */

#include <iostream>
#include "objects.hpp"
using namespace std;
struct Usager {
	string prenom, nom;
	unsigned age;
	void afficher()
	{
		cout<<prenom<<" "<<nom<<" a "<< this->age<< " ans " <<endl;
	}
};


void basesDeLobject(void)
{
	cout<<"la programmation orient� objet"<<endl;
	Usager u;
	u.prenom =" toto";
	u.nom = "Fouad";
	u.age = 100;
	cout<<"Pr�nom "<<u.prenom<<" Nom "<<u.nom<<" age "<<u.age<<endl;

	Usager* u1 = new Usager;
	u1->nom = "Antoine";
	u1->prenom = "Saul";
	u1->age = 70;

	//cout<<"Pr�nom "<<u1->prenom<<" Nom "<<u1->nom<<" age "<<u1->age<<endl;
	u1->afficher();
	delete u1;
}
void Employe::afficher()
{
	cout<<this->nom<<" ( "<<id<<" )"<<endl;
}
Employe::Employe() : nom("Tarik"), id(1000)
{

}
Employe::Employe(string n) : nom(n), id(1000)
{

}
Employe::Employe(string n, int id) : nom(n), id(id)
{

}
Employe::~Employe()
{
	cout<<"del "<<endl;
}
Equipe::Equipe() : Equipe(0) {}
Equipe::Equipe(unsigned t): taille(t),
		employes(new Employe[t]) {}
Equipe::~Equipe() { delete [] employes;}
Equipe::Equipe(const Equipe& src) : taille(src.taille),
		employes( new Employe[src.taille])
{
	for (unsigned i = 0; i<taille;i++)
		this->employes[i] = src.employes[i];
}
Equipe::Equipe(Equipe&& src) :
		taille(src.taille), employes(src.employes)
{
	src.employes  = nullptr;
}
Equipe& Equipe::operator = (const Equipe& src)
{
	if (this != &src)
	{
		this->taille = src.taille;
		this->employes = new Employe[src.taille];
		for (unsigned i = 0; i<taille;i++)
			this->employes[i] = src.employes[i];

	}
	return *this;
}
void objectsComplets()
{
	Employe u2;
	u2.afficher();

	Employe u3("Ismail");
	u3.afficher();

	Employe u4("Farid",10025);
	u4.afficher();

}
void Equipe::afficher()
{
	for(unsigned i = 0; i < taille; i++)
	{
		cout<< " - ";
		employes[i].afficher();
	}
}
void formeCanonique()
{
	Equipe eq1 { 3 };

	eq1.employes[0] = Employe("Tom", 1092);
	eq1.employes[1] = Employe("Beate", 1092);


	eq1.employes[2].nom = "Klinz";
	eq1.employes[2].id = 1098;
	eq1.afficher();

	Equipe eq2 { eq1 }; // constructeur par reco
	Equipe eq3;
	eq3 = eq2;

	Equipe eq4 = eq1; //ocnteur par recopie
	eq4.afficher();

}
Equipe getLequipeAMoi()
{
	Equipe eq { 1 };
	eq.employes[0].nom = "Moi";
	eq.employes[0].id = 2000;
	return eq;
}
