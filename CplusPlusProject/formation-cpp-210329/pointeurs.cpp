#include <iostream>

#include "pointeurs.h"

using namespace std;

void pointeursEtTableaux() {
    cout << "Pointeurs : " << endl;
    unsigned a = 27;
    unsigned* pa = &a;
    cout << "a vaut " << a << ", est � l'adresse " << pa << endl;
    cout << "pa est � l'adresse : " << &pa << ", proche de &a" << endl;
    unsigned** ppa = &pa;
    cout << "ou en passant par une nouvelle variable : " << ppa << endl;

    cout << "a, pa et ppa : " << a << " " << pa << " " << ppa << endl;
    a++;
    cout << "a, pa et ppa : " << a << " " << pa << " " << ppa << endl; //a a chang�

    // interdit : pa = & 4203;
    // interdit : pa = & (a + 5);
    cout << "Ce qu'il y a � l'adresse pa : " << *pa << endl; //28
    // interdit : cout << "Ce qu'il y a � l'adresse a : " << *a << endl; // ?!

    float tf[5] { 0, 2, 4, 6, 8 };
    float* pf = nullptr;
    if(pf != nullptr)
        cout << "Valeur derri�re pf : " << *pf << endl;
    // �quivaut � : pf = &tf;
    // �quivaut � : pf = &tf[0];
    pf = tf;
    cout << "4eme valeur du tableau : " << tf[3] << endl; //6
    cout << "4eme valeur du tableau : " << *(pf+3) << endl; //6
    pf = pf + 3;
    cout << "4eme valeur du tableau : " << *pf << endl; //6

    // afficher la somme des valeurs du tableau sans utiliser de []
    float total = 0;
    for(pf = tf ; pf < tf+5 ; pf++)
        total += *pf;
    cout << "Total : " << total << endl;
}

// unsigned minAnnee(unsigned annees[6]) {
// unsigned minAnnee(unsigned annees[]) {
unsigned minAnnee(unsigned * annees, unsigned taille) {
    unsigned min = 2100;
    //interdit car taille inconnue : for(unsigned a : annees)
    for(unsigned i = 0 ; i < taille ; i++)
        if(annees[i]<min)
            min = annees[i];
    return min;
}

// ======statique=====*****tas****              ||||pile||||
// dans la pile : �chec
unsigned* anciennetesPile(unsigned * annees, const unsigned taille) {
    unsigned res[taille]; // pas const expr !?
    for(unsigned i = 0 ; i < taille ; i++)
        res[i] = 2021 - annees[i];
    return res; // pointeur sur pile renvoy� !?
}

// dans la zone statique
unsigned* anciennetesStatique(unsigned * annees) {
    static unsigned res[6];
    for(unsigned i = 0 ; i < 6 ; i++)
        res[i] = 2021 - annees[i];
    return res;
}

// dans le tas
unsigned* anciennetesTas(unsigned * annees, const unsigned taille) {
    unsigned* res = new unsigned[taille];
    for(unsigned i = 0 ; i < 6 ; i++)
        res[i] = 2021 - annees[i];
    return res;
}

void tableauxEtFonctions() {
    cout << "Tableaux et fonctions " << endl;
    unsigned antennes[] { 1967, 1966, 1982, 2002, 1987, 2004} ;
    cout << "Ouverture : " << minAnnee(antennes, 6) << endl;
    // unsigned* ans = anciennetesPile(antennes, 6); // crash !
    unsigned* ansStatique = anciennetesStatique(antennes); // ok
    for(unsigned i = 0 ; i < 6 ; i++)
        cout << ansStatique[i] << " ";

    unsigned* ansTas = anciennetesTas(antennes,6); // ok
    for(unsigned i = 0 ; i < 6 ; i++)
        cout << ansTas[i] << " ";
    delete[] ansTas;
}

void afficheBlueRays(unsigned& bluerays) {
    bluerays = 10 * unsigned(bluerays / 10);
    cout << "** Bluerays : " << bluerays << endl;
}

unsigned& arrondiBlueRaysA25Pret(unsigned& bluerays) {
    bluerays = 25 * unsigned(bluerays / 25);
    return bluerays;
}

unsigned getHeureOuverture() { return 9; }

string mettreEnForme(const string& s) {
    string resultat = " - " + s + " - ";
    return resultat;
}

string mettreEnForme(string&& s) {
    s = " - " + s + " - ";
    return s;
}

void references() {
    unsigned bluerays = 3245;
    unsigned& br2 = bluerays;
    bluerays += 481; //bluerays : lvalue, 481 : rvalue
    cout << "Bluerays : " << br2 << endl; // 3726
    afficheBlueRays(bluerays);
    cout << "Bluerays : " << br2 << endl; // 3720 !
    // interdit : afficheBlueRays(10000);
    // interdit : afficheBlueRays(bluerays + 5);
    // interdit : unsigned& br3;    br3 = bluerays;
    unsigned& br4 = arrondiBlueRaysA25Pret(bluerays);
    cout << "Bluerays (~25) " << br4 << endl; // 3700
    cout << "Bluerays (~25) " << bluerays << endl; // 3700 ?!

    unsigned h = getHeureOuverture();
    // interdit : pas de rvalue dans ref unsigned& h2 = getHeureOuverture();
    unsigned&& h2 = getHeureOuverture(); // reference � droite
    string titre = "C++ avanc� (2eme edition)";
    string misEnForme = mettreEnForme(titre);
    cout << titre << " => " << misEnForme << endl;
    cout << mettreEnForme("La programmation c'est facile") << endl;
}

