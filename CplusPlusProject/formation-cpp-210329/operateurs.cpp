#include <iostream>

#include "operateurs.h"

using namespace std;

// Code de la classe Rayon :
Rayon::Rayon(): Rayon("", 0) {}
Rayon::Rayon(const string n, const unsigned o):
    nom(n), ouvrages(o) {}
void Rayon::afficher() const {
    cout << nom << " (" << ouvrages << ")" << endl;
}

// r1+r2 : this = &r1 et src2 = r2
Rayon Rayon::operator+(const Rayon & src2) const {
    Rayon res (this->nom+"/"+src2.nom,
        this->ouvrages+src2.ouvrages);
    return res;
}
Rayon Rayon::operator+(const string src2) const {
    Rayon res (this->nom+"/"+src2,
        this->ouvrages);
    return res;
}

bool Rayon::operator==(const Rayon & src2) const {
    return this->nom == src2.nom;
}
bool Rayon::operator!=(const Rayon & src2) const {
    // return this->nom != src2.nom;
    return !(*this==src2);
}

void Rayon::operator()() const {
    afficher();
}

void visibiliteEtOperateurs() {
    cout << "Visibilit� et operateurs" << endl;
    // test de la classe Rayon :
    Rayon r1 { "Geographie", 8711 };
    r1.ouvrages += 62; // ok : dans une amie de Rayon
    r1.afficher();
    Rayon r2 { "Sciences", 4202 };
    // naif : Rayon r3 = r1.operator+(r2);
    Rayon r3 = r1+r2;
    r3.afficher();
    if(r3==r1)
        cout << "R3 et R1 �gaux" << endl;
    if(r3!=r1)
        cout << "R3 et R1 diff�rents" << endl;
    r1(); // afficher r1
}
