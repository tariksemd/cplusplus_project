#include <iostream>

using namespace std;

class Affichable { // abstraite (donc pas final)
public:
    virtual void afficher() = 0; // méthode purement virtuelle
};

class Vehicule : public Affichable {
public:
    Vehicule() = default; // force l'implémentation autom. de ce constructeur
    Vehicule(const Vehicule&) = delete; // empêche l'implémentation automatique
    Vehicule(string n): nom(n) {}
    virtual void afficher() override { cout << nom << " "; }
    void afficherJoli() { cout << "## "; afficher(); cout << "##"; }
protected: // accessible depuis la classe et ses descendantes
    void videNom() { nom = ""; }
    string nom;
};

class Camionnette /* final */ : virtual public Vehicule {
public:
    unsigned livres;
    Camionnette(string n, unsigned l): Vehicule(n), livres(l) {}
    //void afficher() { cout << nom << " (" << livres << ") "; }
    void afficher() /* final */ override { Vehicule::afficher(); cout << " (" << livres << ") "; }
    using Vehicule::videNom; // rend public puisqu'on est dans une zone public
};

class Bus : virtual public Vehicule {
public:
    unsigned accueil;
    Bus(string n, unsigned a) : Vehicule(n), accueil(a) { }
    void afficher() override {
        Vehicule::afficher(); cout << "(max : " << accueil << ") ";
    }
};

class Bibliobus : public Camionnette, public Bus {
public:
    bool actif;
    Bibliobus(string n, unsigned l, unsigned acc, bool act) :
        Vehicule(n), Camionnette(n,l), Bus(n,acc), actif(act) {}
    void afficher() override {
        Camionnette::afficher(); Bus::afficher();
        if(actif) cout << " * actif * "; else cout << " inactif ";
    }
};

void heritageSimple() {
    cout << "Heritage : " << endl;
    {
        Vehicule v1("Peugeot 404");
        v1.afficher();
        v1.afficherJoli();
        cout << endl;
        Camionnette c1("Citroen C4", 380);
        c1.afficher();
        // early binding : n'affiche pas le nombre de livres ?!
        // virtual -> late binding : affiche le nombre de livres
        c1.afficherJoli();
        c1.videNom();
        cout << endl;
        // interdit car abstraite : Affichable a1;
        Bus b1("L6", 54);
        b1.afficher();
        cout << endl;
    }
    Bibliobus bb1("Littérature jeunesse", 420, 8, true);
    bb1.Camionnette::afficherJoli();
    cout << endl;
    // non virtuel : 40, 48, 48, 96
    // virtuel : 40, 56, 56, 72
    cout << sizeof(Vehicule) << ", " << sizeof(Camionnette) << ", " <<
        sizeof(Bus) << ", " << sizeof(Bibliobus) << endl;
}
// ** Classes virtuelles **
// [ actif [Camionnette [Vehicule]] [Bus [Vehicule]]
// =>
// [actif [Camionnette] [Bus] [Vehicule] ]
//
//           Vehicule   Vehicule                 Vehicule
//                 \      /                        /  \
//         Camionnette  Bus       =>     Camionnette  Bus
//                  \   /                         \   /
//                 Bibliobus                    Bibliobus
