#include <iostream>

#include "templates.h"

using namespace std;

//const int CTE = int(35);
template<typename T> const T PI = T(3.14159265352847);

template<typename T> void afficher(T x, T y) { cout << x << " et " << y << endl; }
template<> void afficher<int>(int x, int y) { cout << x << " and " << y << endl; }
template<> void afficher<unsigned>(unsigned x, unsigned y) { cout << x << " und " << y << endl; }

template<typename T> T getX() { cout << "x ?"; T x; cin >> x; return x; }
template<class T> T somme(T a, T b, T c) { return a+b+c; }
template<class T1, class T2> decltype(auto) mult(T1 a, T2 b) { return a*b; }
template<class T1, class T2> auto mult2(T1 a, T2 b)->decltype(a*b) { return a*b; }
// marche pas : template<> string mult2(string a, unsigned b) { return ""; }

template<typename T> struct Conteneur {
    T v;
    Conteneur() : v() {}
    Conteneur(T arg): v(arg) {}
    template<typename T2> bool egalApproximatif(T2 n) { return (n>v-1) && (n<v+1); }
};
template<> struct Conteneur<string> {
    std::string v;
    Conteneur() : v("inconnue") {}
    Conteneur(string  arg): v(arg) {}
    // interdit : template<typename T2> bool egalApproximatif(T2 n) { return (n>v-1) && (n<v+1); }
};

// sp�cialisation partielle :
// template<typename T1, typename T2> struct S {.....
// template<typename T1> struct S<T1,string> {.....
// template<typename T2> struct S<int,T2> {.....
// template<> struct S<int,double> {....

namespace fr_ibformation {
    namespace toulouse {
        template<int V> unsigned multiplieur(unsigned x) { return x*V; }
    }
}
//using namespace formation_ib;
void templates() {
    // recherche de cout : mot-cl� puis "templates" puis namespace global puis namespace "std"
    cout << "Templates : " << endl;
    float pif = PI<float>;
    cout << "float: " << pif << endl;
    cout << "double: " << PI<double> << endl;
    cout << "unsigned: " << PI<unsigned> << endl;
    // cout << "string: " << PI<string> << endl; // interdit : string(3.1415)
    afficher<int>(4, 12);
    afficher("Ann", "Bob");
    afficher<double>(6, 8.3);
    // int v = getX<int>();  cout << v << endl;
    cout << somme(string("a"), string("b"), string("c")) << " easy as " << somme(1,2,3) << endl; // "abc easy as 6"
    cout << mult(4.3, 7.2) << " " << mult(6, 3.4) << " " << mult(3.4, 6) << endl; // 30.96 20.4 20.4
    cout << mult2(4.3, 7.2) << " " << mult2(6, 3.4) << " " << mult2(3.4, 6) << endl; // 30.96 20.4 20.4
    // pas de * dans std::string - cout << mult(string("a"),3) << endl;
    Conteneur<float> c1;
    c1.v = 2.3199f;
    Conteneur<float> c2(3.4f);
    cout << "Conteneurs : " << c1.v << " " << c2.v << " " << c2.egalApproximatif(4.5) << endl;
    Conteneur<string> c3;
    cout << "Conteneur 3 : " << c3.v << endl; // "Conteneur 3 : inconnue"
    cout << "Template sur entier : " << fr_ibformation::toulouse::multiplieur<2+2>(5) << endl; // 20
}
