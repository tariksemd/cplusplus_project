#include <iostream>
#include <vector>
#include <array>
#include <list>
#include <deque>
#include <forward_list>
#include <map>
#include <algorithm>
#include <memory>

#include "stl.h"

using namespace std;

bool assezLong(string s){ return s.size()>=8; }

void collections() {
    cout << "Collections : " << endl;
    // appel du constructeur vector<T>(initializer_list<T>)
    vector<string> titres {"Illiade", "Odyssee", "Le Retour"};
    titres.push_back("La vengeance");
    titres.insert(titres.begin(), "Best of Hom�re");
    titres[0] = "L'" + titres[0];
    cout << titres.size() << " titres" << endl;
    {
        // afficher tous les titres
        for(unsigned i=0 ; i < titres.size(); i++)
            cout << titres[i] << " ";
        for(auto it=titres.cbegin() ; it!=titres.cend() ; it++)
            cout << *it << " ";
        for(const string& s : titres)
            cout << s << " ";
        // cr�er un nouveau vecteur a partir des titres 0, 2, 4....
        vector<string> titresPairs ( titres.size()/2 );
        for(auto it=titres.cbegin() ; it<titres.cend() ; it+=2)
            titresPairs.push_back(*it);
        // array : comme un vecteur, mais taille fixe constexpr
        array<string,3> titresCourts {"Jo", "Fame", "Si"};
        // list : comme un vecteur, mais sans []
        list<string> auteurs {"Hom�re", "Proust", "Zola"};
        // deque : comme un vecteur avec push/pop _ front/back
        deque<long> retours {23209, 13209, 14933};
        // forward_list : comme une liste, mais chaque �l�ment ne cnnait que le suivant
        forward_list<string> autresAuteurs {"Sun zu", "Mao"};
    }
    vector<string> titresLongs ( titres.size() );
    //copy_if(titres.begin(), titres.end(), titresLongs.begin(), assezLong);
    copy_if(titres.begin(), titres.end(), titresLongs.begin(),
            [](string s){ return s.size()>=8; } );
    cout << "Titres longs : " << endl;
    for(const string& s : titresLongs)
        cout << s << " ";

    bool unY = any_of(titres.begin(), titres.end(),
            [](string s){ return s.find('y', 0)!=string::npos; });
    cout << endl << "Un Y dans l'un des titres ? " << unY << endl;

    bool tousCourts = all_of(titres.begin(), titres.end(),
            [](string s){ return s.size() < 8; });
    cout << "Tous - de 8 caract�res ? " << tousCourts << endl;
}

// unordered_/  multi/  set/map, exemples : set, multiset, unordered_multimap
// set : ensemble de cl� ; map : ensemble de cl�+valeurs
// unordered : l'ordre d'insertion n'est pas gard�
// multi : il peut  avoir des doublons
void ensembles() {
    map<unsigned,unsigned> nouveautesParAnnee {{2018, 23}, {2019, 111}, {2020, 34}};
    if(nouveautesParAnnee.count(2018)>0)
        cout << "2018 : " << nouveautesParAnnee[2018] << " nouveaut�s" << endl;
    map<unsigned,array<unsigned,12>> nouveautesParAnneeEtMois;
    nouveautesParAnneeEtMois[2021][3] = 0; // rien ce mois ci
}

void pointeurIntelligents() { // #include <memory>
    cout << "Smart pointers : " << endl;
    string* s1 = new string("Antenne de Borderouge");
    cout << *s1 << endl;
    delete s1;

    unique_ptr<string> s2(new string("Bibliotheque centrale"));
    cout << *s2 << endl;
    // interdit car unique : unique_ptr<string> s3 = s2;
}


