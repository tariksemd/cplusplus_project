#include <iostream> // cherche directement dans les r�pertoires standards

#include "bases.h" // cherche d'abord dans le r�pertoire en cours
#include "bases.h" // inclu en r�alit� une seule fois
#include "bases.h"
#include "fonctions.h"
#include "pointeurs.h"
#include "objets.h"
#include "operateurs.h"
#include "heritage.h"
#include "exceptions.h"
#include "templates.h"
#include "stl.h"

using namespace std;
// using namespace fr_ibformation_toulouse;

// fonction de d�part
int main()
{
    cout << "Debut" << endl;
    // typesEtTestsEtBoucles();
    // parametres();
    // montreOuvrage("Le Mahabaratha");
    // variablesFonctions();
    // pointeursEtTableaux();
    // tableauxEtFonctions();
    // references();
    // basesDeLobjet();
    // objetsComplets();
    // formeCanonique();
    // visibiliteEtOperateurs();
    // heritageSimple();
    // exceptions();
    // templates();
    // collections();
    // ensembles();
    pointeurIntelligents();
    cout << "Fin" << endl;
}
