#ifndef OPERATEURS_H_INCLUDED
#define OPERATEURS_H_INCLUDED

using namespace std;

void visibiliteEtOperateurs();
// -----------------
// |  Rayon        |
// -----------------
// | - nom         |
// | - ouvrages    |
// -----------------
// | + Rayon()     |
// | + Rayon(n, o) |
// | + afficher()  |
// -----------------
class Rayon {
public:
    Rayon();
    Rayon(const string, const unsigned);
    void afficher() const;
    Rayon operator+(const Rayon &) const;
    Rayon operator+(const string) const;
    bool operator==(const Rayon &) const;
    bool operator!=(const Rayon &) const;
    void operator()() const;
private:
    string nom;
    unsigned ouvrages;

    friend void visibiliteEtOperateurs();
};

#endif // OPERATEURS_H_INCLUDED
