#include <iostream>
#include <functional>

#include "objets.h"

using namespace std;

struct Usager {
    string prenom, nom;
    unsigned age;
    void afficher() {
        cout << prenom << " " << nom << " a " << this->age << " ans" << endl;
    }
    void vieillir(int a) {
        age = age+a;
    }
    void rajeunir(int a) {
        vieillir(-a); // ou this->vieillir(-a);
    }
};

void basesDeLobjet() {
    cout << "Classes et objets " << endl;
    Usager u1;
    u1.prenom = "Bob";
    u1.nom = "Martin";
    u1.age = 78;
    //cout << u1.prenom << " " << u1.nom << " a " << u1.age << " ans" << endl;
    u1.vieillir(4);
    u1.rajeunir(2);
    u1.afficher(); // ..... 80
    auto vieillir2 = bind(&Usager::vieillir, u1, placeholders::_1); // ok
    // auto rajeunir2 = bind(&Usager::vieillir, u1, -placeholders::_1); // KO

    Usager * u2 = new Usager;
    //(*u2).prenom = "Ann";
    u2->prenom = "Ann";
    u2->nom = "Martin";
    u2->age = 60;
    // cout << u2->prenom  << " " << u2->nom << " a " << u2->age << " ans" << endl;
    u2->afficher();
    delete u2;
}

void Employe::afficher() {
    cout << this->nom << " (" << id << ")" << endl;
}

// implémentation des constructeurs :
Employe::Employe() : Employe("") {}
Employe::Employe(string n) : Employe(n, 1000) {}
Employe::Employe(string n, int x) : nom(n), id(x) {}

Employe::~Employe() { cout << "del~ " << endl; }

void objetsComplets() {
    // tester la classe Employe
    // codegolf : le + court possible
    // interdit ! déclaration de fonction : Employe u1();
    Employe u1;
    u1.afficher();
    Employe u2("Carol");
    u2.afficher();
    Employe u3("Dana", 1018);
    u3.afficher();

    int n1 = 3;
    int n2(3);

    // ou :
    // Employe u1 {};
    // u1.afficher();
    // Employe u2 {"Carol"};
    // u2.afficher();
    // Employe u3 {"Dana", 1018};
    // u3.afficher();
    // int n3 { 3 };
}

Equipe::Equipe() : Equipe(0) {}
Equipe::Equipe(unsigned t) : taille(t),
        employes(new Employe[t]) {}
Equipe::Equipe(const Equipe& src) : taille(src.taille),
        employes(new Employe[src.taille]) {
    for(unsigned i=0;i<taille;i++)
        this->employes[i] = src.employes[i];
}
Equipe::Equipe(Equipe&& src) :
        taille(src.taille),
        employes(src.employes) {
    src.employes = nullptr;
}
Equipe::~Equipe() { delete [] employes; }

void Equipe::afficher() {
    for(unsigned i=0;i<taille;i++) {
        cout << " - " ;
        employes[i].afficher();
    }
}

Equipe& Equipe::operator=(const Equipe& src) {
    if(this != &src) { // rien faire si eq1 = eq1;
        this->taille = src.taille;
        delete [] this->employes;
        this->employes = new Employe[src.taille];
        for(unsigned i=0;i<taille;i++)
            this->employes[i] = src.employes[i];
    }
    return *this;
}

// Equipe& operator=(Equipe& gauche, const Equipe& droite) {...}
// double operator+(string gauche, int droite) { ... }

Equipe& Equipe::operator=(Equipe&& src) {
    if(this != &src) { // rien faire si eq1 = eq1;
        this->taille = src.taille;
        this->employes = src.employes;
        src.employes = nullptr;
    }
    return *this;
}

Equipe getLequipeAMoi() {
    Equipe eq { 1 };
    eq.employes[0].nom = "Moi";
    eq.employes[0].id = 2000;
    return eq;
}

void formeCanonique() {
    Equipe eq1 { 3 };
    eq1.employes[0] = Employe("Tom", 1092);
    eq1.employes[1] = Employe("Beate", 1093);
    // eq1.employes[2] = Employe("Klinz", 1098);
    // plus économe :
    eq1.employes[2].nom = "Klinz";
    eq1.employes[2].id = 1098;
    eq1.afficher();
    {
        Equipe eq2 { eq1 }; // constructeur par recopie
        Equipe eq3;
        eq3 = eq1; // affectation, opérateur =
        Equipe eq4 = eq1; // constructeur par recopie
        eq1.employes[0].id = 4001; // ne change pas eq4 !
        eq4.afficher();
    }
    Equipe eq5 (getLequipeAMoi()); // constructeur par recopie
    eq5.afficher();
    eq5 = getLequipeAMoi(); // op = par déplacement
}


