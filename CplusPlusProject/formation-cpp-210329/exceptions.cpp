#include <iostream>

#include "exceptions.h"

using namespace std; // associ� � la Standard Template Library

unsigned getBibliothecaires(int visiteurs) noexcept {
    return 1+visiteurs/25;
}

void exceptions() noexcept {
    cout << "Exceptions : " << endl;
    int visiteurs;
    cout << "Visiteurs ? ";
    cin >> visiteurs;
    unsigned tables = 14;
    try {
        if(visiteurs==0)
            throw 0; // les exceptions sont lanc�es par valeur et attrap�es par r�f�rence
        if(visiteurs<0)
            throw out_of_range("Trop petit");
        unsigned tablesParVisiteur = tables / visiteurs;
        cout << "Tables par visiteur : " << tablesParVisiteur << endl;
    } catch(int& codeErreur) {
        cout << "Code d'erreur : " << codeErreur << endl;
    } catch(out_of_range& exc) {
        cout << "Exception - hors limites - : " << exc.what() << endl;
    } catch(...) {
        cout << "Erreur inattendue" << endl;
    }
    if(noexcept(getBibliothecaires(visiteurs))) {
        cout << "Biblioth�caires : " << getBibliothecaires(visiteurs) << endl;
    }
}
