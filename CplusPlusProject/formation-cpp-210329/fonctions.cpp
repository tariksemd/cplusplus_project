#include <iostream>
#include <functional> // C++11

#include "fonctions.h"

using namespace std;

unsigned getNbLivresParRayon(unsigned longueurEnCm, unsigned etageres) {
    return longueurEnCm * etageres / 3;
}
unsigned getNbLivresParRayon(unsigned longueurEnCm, unsigned etageres,
                                unsigned largeurDunLivre) {
    return longueurEnCm * etageres / largeurDunLivre;
}
unsigned getNbLivresParRayon(unsigned longueurEnCm) {
    return longueurEnCm/ 3;
}
unsigned getNbLivresParRayon(float longueurEnM, unsigned etageres) {
    return unsigned(longueurEnM * 100 * etageres / 3);
}
unsigned getNbLivresParRayon(double longueurEnM, unsigned etageres) {
    return unsigned(longueurEnM * 100 * etageres / 3);
}
/* nom d'argument : insuffisant
unsigned getNbLivresParRayon(unsigned longueurEnM, unsigned etageres) {
    return longueurEnM * 100 * etageres / 3;
} */
/* type de retour : insuffisant
double getNbLivresParRayon(unsigned longueurEnCm, unsigned etageres) {
    return longueurEnCm * etageres / 3.0;
} */
auto getNbLivresParRayonDbl(unsigned longueurEnCm, unsigned etageres) {
    return longueurEnCm * etageres / 3.0;
}

void montreOuvrage(const string titre, const string auteur, const unsigned pages) {
    cout << "**" << titre << "** de " << auteur << " (" << pages << ")" << endl;
    // interdit par "const" : pages = 5;
}

void montreInfosBibli(int nbInfos, ...) {
    // code de lecture
}

void parametres() {
    cout << "Param�tres de fonctions" << endl;
    cout << "4m de long, 4 �tag�res : " <<
        getNbLivresParRayon(unsigned(400), 4) << " livres" << endl;
    cout << "4m de long, 4 �tag�res, gros livres : " <<
        getNbLivresParRayon(400, 4, 7) << " livres" << endl;
    montreOuvrage("L'Odyssee");
    montreOuvrage("L'Illiade", "Homere");
    montreOuvrage("Mes m�moires", "Homere", 24);
}

// #include <functional> // C++11
void variablesFonctions() {
    //function<void(string,string,unsigned)> f1 = montreOuvrage;
    auto f1 = montreOuvrage;
    f1("Asterix aux Jeux Olympiques", "Uderzo, Goscinny", 44);

    function<void()> f2 = bind(montreOuvrage, "Asterix le Gaulois", "Uderzo", 36);
    f2();
    function<void(unsigned)> f3 = bind(montreOuvrage, "Asterix", "Uderzo",
                                    placeholders::_1);
    f3(42);
    function<void(string)> f4 = bind(montreOuvrage, placeholders::_1, "Uderzo", 44);
    f4("Asterix et Cleopatre"); // 44 pages
    function<void(unsigned, string)> f5 =
        bind(montreOuvrage, placeholders::_2, "Uderzo, Goscinny", placeholders::_1);
    f5(42, "La Zizanie");
}
